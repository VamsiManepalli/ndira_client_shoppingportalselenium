package com.ndira.sponsor_client.portal.tests;

import java.io.IOException;
import org.testng.annotations.Test;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;
import com.ndira.sponsor_client.portal.libraries.LoginLogout;
import com.ndira.sponsor_client.portal.libraries.ClientTransaction_Validation;
import com.ndira.sponsor_client.portal.libraries.ClientName_Validation;

public class Ndira_sponsor_client_Test extends NDira_sponsor_client_Constants
{
	NDira_sponsor_client_XLUtils xl = new NDira_sponsor_client_XLUtils();
	String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	String tssheet = "TestSteps";
	int tccount, tscount;
	String tcexe;
	String tcid, tsid, keyword;
	String tcres = "";
	boolean res = false;
	
	ClientName_Validation s=new ClientName_Validation();
	ClientTransaction_Validation s1=new ClientTransaction_Validation();
	LoginLogout li=new LoginLogout();
	
	@Test
	public void ndira_sponsor_client_test() throws IOException 
	{
		tccount = xl.getRowCount(xlfile, tcsheet);
		tscount = xl.getRowCount(xlfile, tssheet);
		for (int i = 1; i <= tccount; i++)
		{
			tcexe = xl.getCellData(xlfile, tcsheet, i, 2);
			if (tcexe.equalsIgnoreCase("Y"))
			{
				tcid = xl.getCellData(xlfile, tcsheet, i, 0);
				for (int j = 1; j <= tscount; j++) 
				{
					try
					{
						tsid = xl.getCellData(xlfile, tssheet, j, 0);
					} 
					catch (Exception e) 
					{
						System.out.println("");
					}
					if (tcid.equalsIgnoreCase(tsid)) 
					{
						keyword = xl.getCellData(xlfile, tssheet, j, 4);

						switch (keyword.toUpperCase()) 
						{
						
						case "CLIENTNAMEVALIDATION":
							s.uname=xl.getCellData(xlfile, tssheet, 1, 9);
							s.password=xl.getCellData(xlfile, tssheet, 1, 12);
							res=s.sample();							
							li.sponsorlogout();
							break;
						
						case "CLIENTTRANSACTIONVALIDATION":
							res=s1.sample1();
							break;
						
							
							
							

						default:
							break;
						}

						String tsres = null;

						if (res)
						{
							tsres = "Pass";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillGreenColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillGreenColor(xlfile, tcsheet, i, 3);
						} 
						else 
						{
							tsres = "Fail";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillRedColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillRedColor(xlfile, tcsheet, i, 3);
						}
					}
				}
			} 
			else 
			{
				xl.setCellData(xlfile, tcsheet, i, 3, "Not Executed");
				xl.fillOrangeColor(xlfile, tcsheet, i, 3);
				
			}
		}
	}

}
