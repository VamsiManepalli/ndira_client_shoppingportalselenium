package com.ndira.sponsor_client.portal.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;



	public class NDira_sponsor_client_Constants
	{
		//public static String path="C:\\Users\\ADMIN\\Downloads\\chromedriver.exe";		
	    //public static String c=System.setProperty("webdriver.chrome.driver", path);
		//public static WebDriver d=new ChromeDriver();
	   
		public static WebDriver d=new FirefoxDriver();
	    public static String url=NDira_sponsor_client_Propertiesfile.getProperty("url");
	    public static String shopping_cart_url=NDira_sponsor_client_Propertiesfile.getProperty("shopping.cart.url");
	  
	  @BeforeTest
	  public void Launch()
	  {
		  try 
		  {
		   d.get(url);
		   d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   d.manage().window().maximize();
		  } 
		  catch (Exception e)
		  {
				System.out.println(e);
		  }
	  }
	@AfterTest
	 public void Close()
	 {
		d.close();
	 }
	
	}


