package com.ndira.sponsor_client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;


public class LoginLogout extends NDira_sponsor_client_Constants
{

	
	public void Clientlogin() throws IOException
	{
		
		NDira_sponsor_client_XLUtils xl = new NDira_sponsor_client_XLUtils();
		String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
		String tssheet = "TestSteps";
		
		//From Excel
		String uname=xl.getCellData(xlfile, tssheet, 2, 7);
		String password=xl.getCellData(xlfile, tssheet, 2, 8);
		
			
		d.findElement(By.id("input-login")).sendKeys(uname);
		d.findElement(By.id("input-password")).sendKeys(password);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='login-view']/form/div[3]/button")).click();
	}
	public void Sponsorlogin() throws IOException
	{
		
		NDira_sponsor_client_XLUtils xl = new NDira_sponsor_client_XLUtils();
		String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
		String tssheet = "TestSteps";
		
		//From Excel
		String uname=xl.getCellData(xlfile, tssheet, 1, 9);
		String password=xl.getCellData(xlfile, tssheet, 1, 12);		
			
		d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(uname);
		d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(password);
		d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	public void sponsorlogout() throws IOException
	{				
		d.findElement(By.linkText("Log out")).click();
	}

}
