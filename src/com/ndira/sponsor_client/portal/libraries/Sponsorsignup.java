package com.ndira.sponsor_client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;

public class Sponsorsignup extends NDira_sponsor_client_Constants
{
	
	public static void sponserSignup() throws IOException
	{
		try
		{		
			NDira_sponsor_client_XLUtils xl=new NDira_sponsor_client_XLUtils();
			String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			int rownumber=1;
			String company,Category,fname,lname,email,number,password,cpass,howdiduknow;
			company=xl.getCellData(xlfile, tssheet, rownumber, 5);
			Category=xl.getCellData(xlfile, tssheet, rownumber, 6);
			fname=xl.getCellData(xlfile, tssheet, rownumber, 7);
			lname=xl.getCellData(xlfile, tssheet, rownumber, 8);
			email=xl.getCellData(xlfile, tssheet, rownumber, 9);
			number=xl.getCellData(xlfile, tssheet, rownumber, 10);
			howdiduknow=xl.getCellData(xlfile, tssheet, rownumber, 11);
			password=xl.getCellData(xlfile, tssheet, rownumber, 12);
			cpass=xl.getCellData(xlfile, tssheet, rownumber, 13);
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/div/div/a")).click();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.id("company")).sendKeys(company);
			Select cat=new Select(d.findElement(By.id("primary-investment-category")));
			cat.selectByVisibleText(Category);
			d.findElement(By.id("name-first")).sendKeys(fname);
			
			d.findElement(By.id("name-last")).sendKeys(lname);
			
			d.findElement(By.id("email")).sendKeys(email);
			
			d.findElement(By.id("phone-1")).sendKeys(number);
			
			Select abc=new Select(d.findElement(By.id("sponsor-how-did-you-know")));
			abc.selectByIndex(1);
			
			d.findElement(By.id("password")).sendKeys(password);
			
			d.findElement(By.id("password-again")).sendKeys(cpass);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[9]/div/div/button")).click();
						
		} 
		catch (Exception e)
		{
			System.out.println(e);
		}
			
	}

}
