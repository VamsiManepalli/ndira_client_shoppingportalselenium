package com.ndira.sponsor_client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;

public class ClientSignup extends NDira_sponsor_client_Constants

{	
	public static void clientSignup() throws IOException
	{
		try
		{
		NDira_sponsor_client_XLUtils xl=new NDira_sponsor_client_XLUtils();
		String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		
		//d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys("apuram@sanela.in");
		//d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys("Sanela@123");
		//d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Sleeper.sleepTightInSeconds(5);
		d.findElement(By.linkText("Client Sign Up")).click();
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Sleeper.sleepTightInSeconds(2);
		String clienturl=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div[2]/div/p[2]/a")).getText();
		d.get(clienturl);
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		int rownumber=2;
		String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,investiment,phonen,cardno,cardExpiryDate,cardName,cardCVCNumber,AccNo;
		fname=xl.getCellData(xlfile, tssheet, rownumber, 5);
		lname=xl.getCellData(xlfile, tssheet, rownumber, 6);
		email=xl.getCellData(xlfile, tssheet, rownumber, 7);
		pass=xl.getCellData(xlfile, tssheet, rownumber, 8);
		cpass=xl.getCellData(xlfile, tssheet, rownumber, 9);
		DOB=xl.getCellData(xlfile, tssheet, rownumber, 10);
		SSN=xl.getCellData(xlfile, tssheet, rownumber, 11);
		maritalstatus=xl.getCellData(xlfile, tssheet, rownumber, 12);
		phone=xl.getCellData(xlfile, tssheet, rownumber, 13);
		address1=xl.getCellData(xlfile, tssheet, rownumber, 14);
		address2=xl.getCellData(xlfile, tssheet, rownumber, 15);
		city=xl.getCellData(xlfile, tssheet, rownumber, 16);
		state=xl.getCellData(xlfile, tssheet, rownumber, 17);
		zipcode=xl.getCellData(xlfile, tssheet, rownumber,18);
		typeofaccount=xl.getCellData(xlfile, tssheet, rownumber, 19);
		investiment=xl.getCellData(xlfile, tssheet, rownumber, 20);
		phonen=xl.getCellData(xlfile, tssheet, rownumber, 21);
		cardno=xl.getCellData(xlfile, tssheet, rownumber, 22);
		cardExpiryDate=xl.getCellData(xlfile, tssheet, rownumber, 23);
		cardName=xl.getCellData(xlfile, tssheet, rownumber, 24);
		cardCVCNumber=xl.getCellData(xlfile, tssheet, rownumber, 25);
		AccNo=xl.getCellData(xlfile, tssheet, rownumber, 26);
		
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		  
		  d.findElement(By.id("firstName")).sendKeys(fname);		  
		  d.findElement(By.id("lastName")).sendKeys(lname);		 
		  d.findElement(By.id("emailAddress")).sendKeys(email);		  
		  d.findElement(By.id("password")).sendKeys(pass);		  
		  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
		  d.findElement(By.id("nextButton")).click();
		  //Step2
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
		  d.findElement(By.id("SSN")).sendKeys(SSN);
		  Select ms=new Select(d.findElement(By.id("maritalStatus")));
		  ms.selectByVisibleText(maritalstatus);
		  d.findElement(By.id("phone")).sendKeys(phone);
		  d.findElement(By.id("street")).sendKeys(address1);
		  d.findElement(By.id("aptSte")).sendKeys(address2);
		  d.findElement(By.id("city")).sendKeys(city);
		  Select st=new Select(d.findElement(By.id("state")));
		  st.selectByVisibleText(state);
		  d.findElement(By.id("zip")).sendKeys(zipcode);
		  Select toa=new Select(d.findElement(By.id("accountType")));
		  toa.selectByVisibleText(typeofaccount);
		  //d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[5]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
		  d.findElement(By.xpath("html/body/div[3]/form/div[8]/div[2]/div[2]/label/span")).click();
		  d.findElement(By.id("nextButton")).click();
		 
		  //Step3
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		  
		  d.findElement(By.id("investmentAmount")).sendKeys(investiment);
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.className("c-indicator")).click();
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		  d.findElement(By.id("withDrawFeesTransaction")).click();
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		  d.findElement(By.id("iAgreeInvestment")).click();
		 // d.findElement(By.xpath("html/body/div[3]/form/div[4]/div[2]/div[2]/label/span")).click();
		  d.findElement(By.id("nextButton")).click();
		  
		  //Step4
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		  d.findElement(By.id("cardNumber")).sendKeys(cardno);
		  d.findElement(By.id("cardExpiryDate")).sendKeys(cardExpiryDate);
		  d.findElement(By.id("cardName")).sendKeys(cardName);
		  d.findElement(By.id("cardCVCNumber")).sendKeys(cardCVCNumber);
		  d.findElement(By.xpath("html/body/div[3]/form/div[5]/div[2]/div[2]/label[1]/span")).click();
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span[1]")).click();
		  d.findElement(By.id("nextButton")).click();
		 
		  //AccNo=d.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div/p[2]")).getText();
		  //xl.setCellData(xlfile, tssheet, 13, 26, AccNo);
		  
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
		  
	 }
}
