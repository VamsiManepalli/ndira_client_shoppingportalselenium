package com.ndira.sponsor_client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;

public class ClientTransaction_Validation extends NDira_sponsor_client_Constants
{
	public static boolean sample1() throws IOException 
	{
		try 
			{
				NDira_sponsor_client_XLUtils xl=new NDira_sponsor_client_XLUtils();
				String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
				String tcsheet="TestCases";
				String tssheet="TestSteps";
				
				d.get(shopping_cart_url);
				d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				String item;
				item=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[1]/p/b")).getText();
				d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
				d.switchTo().alert().accept();
				d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
				d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				String cartitem=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/table/tfoot/tr[1]/th[1]/input")).getAttribute("value");
				if (item.contains(cartitem)) 
				{
					d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
					Select PaymentSelection=new Select(d.findElement(By.xpath(".//*[@id='repeatSelect']")));
					PaymentSelection.selectByVisibleText("New Direction IRA Account");
					
					d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/button")).click();
					
					//login
					d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					LoginLogout li=new LoginLogout();
					li.Clientlogin();
				}
				d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				Select accno=new Select(d.findElement(By.id("repeatSelect")));
				accno.selectByIndex(1);
				
				 d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/button")).click();
				 
				 d.get(url);
				 
				 d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				 LoginLogout li=new LoginLogout();
				 li.Sponsorlogin();
				 
				 d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				 d.findElement(By.linkText("Transactions")).click();
				 String expname,acname;
				 expname=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[1]/ui-view/div/div[3]/div/div/table[2]/tbody/tr[1]/td[2]/a")).getText();
				 acname=xl.getCellData(xlfile, tssheet, 2, 5);
				 
				
				 if (expname.contains(acname))
				 {
					return true;
				 } 
				 else
				 {
					return false;
				 }
			
			} 
			catch (Exception e)
			{
				System.out.println(e);
				return false;
			}
	}
}
