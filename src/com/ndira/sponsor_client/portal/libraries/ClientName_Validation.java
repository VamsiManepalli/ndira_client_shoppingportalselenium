package com.ndira.sponsor_client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Constants;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_Propertiesfile;
import com.ndira.sponsor_client.portal.utils.NDira_sponsor_client_XLUtils;

public class ClientName_Validation extends NDira_sponsor_client_Constants
{
 public static String uname,password;
 public boolean sample() throws IOException
 {
	 try 
	 {
		NDira_sponsor_client_XLUtils xl=new NDira_sponsor_client_XLUtils();
		String xlfile = NDira_sponsor_client_Propertiesfile.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
	    
		//Sponsor Signup
		Sponsorsignup su=new Sponsorsignup();
	    su.sponserSignup();	
		
	    d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    Sleeper.sleepTightInSeconds(2);
	 
		 ClientSignup cu=new ClientSignup();
		 cu.clientSignup();
		 
		 d.get(url);	 
		 d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 d.findElement(By.linkText("Accounts")).click();
		 d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 
		 String expname,acname;
		 expname=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[1]/ui-view/div/div[3]/div/div/table/tbody/tr[1]/td[1]/a")).getText();
		 acname=xl.getCellData(xlfile, tssheet, 2, 5);
		 
		 if (expname.contains(acname))
		 {
			return true;
		 } 
		 else
		 {
			 return false;
		 }
			
	} 
	catch (Exception e)
	 {
		System.out.println(e);
		return false;
	}
  }
}
